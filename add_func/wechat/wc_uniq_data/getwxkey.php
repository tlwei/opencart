<?php

// include_once "test.php";

// base class with member properties and methods

if(isset($_POST['js_code'])){
$data = array(
    "appid" => $_POST['appid'],
    "secret" => $_POST['secret'],
    "js_code" => $_POST['js_code'],
    "grant_type" => $_POST['grant_type']
);
$data_string = $data;

$ch = curl_init('https://api.weixin.qq.com/sns/jscode2session');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Accept'=> 'application/json',
    'content-type'=> 'application/x-www-form-urlencoded'
)
);

echo curl_exec($ch);

}else{
    echo "no permission.";
}